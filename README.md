# CAFContainer

## Running the HWWAnalysisCode on NEMO with containers

In the following we will use docker containers to build the HWWAnalysisCode locally,
and submit the analysis jobs from the local container to the NEMO cluster. 

First install docker, see [https://docs.docker.com/install/](https://docs.docker.com/install/)

To have the atlas enviroment availiable on the local machine, we will use the
docker container "atlas/analysisbase", which is available on dockerhub.
Use the `docker pull` command to load the container.

```
$ docker pull atlas/analysisbase:latest
```

Create a work directory in your favorite location, download the project from gitlab and create
a run and build directory. 

```
$ git clone --recursive --branch=HWWlvqq_Johannes https://gitlab.cern.ch/atlas-physics/higgs/hww/HWWAnalysisCode.git
$ mkdir run build
```

To submit the analysis later we will use ssh from within the container, so lets create a rsa key pair.

```
$ mkdir ~/.sshc
$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/a/.ssh/id_rsa): /home/$(whoami)/.sshc/id_rsa
Created directory '/home/a/.sshc'.
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/a/.sshc/id_rsa.
Your public key has been saved in /home/a/.sshc/id_rsa.pub.
The key fingerprint is:
...

$ ssh <nemo-user>@login1.nemo.uni-freiburg.de mkdir -p .ssh
$ cat .sshc/id_rsa.pub | ssh <nemo-user>@login1.nemo.uni-freiburg.de 'cat >> .ssh/authorized_keys'
```

The key is stored in the non-standard folder ~/.sshc since the user in the container needs to take ownership of this 
location.

Now we can start the container and build the HWWAnalysisCode.
Execute the following in your work directory. 

```
$ ls 
build HWWAnalysisCode run

$ docker run --rm -it -v $PWD:/hww -v /home/<user>/.sshc:/home/atlas/.ssh -w /hww atlas/analysisbase:latest

container> source ~/release_setup.sh
container> sudo chown -R atlas ~/.ssh
container> sudo chown -R atlas .
container> cd build
container> cmake ../HWWAnalysisCode
container> make -j5
container> sed -i 's*atlas/jh557-HWWlvqq_candRebuild*nemo/fr_jm484-hwwanalysis-0*g' ../HWWAnalysisCode/share/config/master/common/campaigns-lvqq.cfg
container> sed -i 's*atlas/jh557-HWWlvqq_candRebuild*nemo/fr_jm484-hwwanalysis-0*g' ../HWWAnalysisCode/share/config/master/common/campaigns.cfg
```

The last two steps are necessary since NEMO-User cannot access the data stored on the BFG, 
so one has to access the data I copied to the NEMO storage.

Now the project is build and you can run the analysis. 
But before we can submit the analysis jobs we have to build a container with the project and make it availiable 
on the cluster. For this purpose one can use either dockerhub or the gitlab.cern.ch registry. 

To build the container create a Dockerfile (in a new terminal) and upload the container. 
Note: To build the container you have to change the ownership of the project back to your user.

```
$ cat >> Dockerfile <<EOL
> FROM atlas/analysisbase:latest
> ADD . /hww/
> WORKDIR /hww/run
> EOL


$ sudo chown -R $(whoami) .
$ docker build -t=hwwanalysis .
```


In preparation for the analysis allocate storage on the NEMO cluster

```
$ ssh <nemo_user>@login1.nemo.uni-freiburg.de
NEMO> export USER=$(whoami)
NEMO> ws_allocate hwwanalysis 100
NEMO> cd /work/ws/nemo/$USER-hwwanalysis-0/
NEMO> cd run
```


There are two ways to make the container availiable on the cluster. 
The first one is to convert the docker-container into a singularity container locally, the second
is to upload the docker container to the gitlab registry.

### Uploading the container to a gitlab registry

If you don't have an existing gitlab project, create a new one, see [https://gitlab.cern.ch/projects/new](https://gitlab.cern.ch/projects/new). 

In order to upload the container to the gitlab registry, login with your CERN username and 
password.
Also rename the container with `docker tag` to match the gitlab project you want to upload the container to.

```
$ docker login gitlab-registry.cern.ch
$ docker tag hwwanalysis gitlab-registry.cern.ch/USERNAME/REPOSITORY/hwwanalysis:latest
$ docker push gitlab-registry.cern.ch/USERNAME/REPOSITORY/hwwanalysis:latest
```


### Converting the container into a singularity container

To convert the docker container into a singularity-container we can make use of the tool [docker2singularity](https://github.com/singularityware/docker2singularity).

```
$ docker run -v /var/run/docker.sock:/var/run/docker.sock -v $PWD:/output --privileged -t --rm singularityware/docker2singularity --name hwwanalysis.simg hwwanalysis:latest
```

This command will create a singularity image with the name 'hwwanalysis.simg' from the docker container hwwanalysis:latest and copy it to the current directory.
Now we can use `scp` to copy the container to the cluster. 

```
$ scp hwwanalysis.simg <nemo-user>@login1.nemo.uni-freiburg.de:/work/ws/nemo/<nemo-user>-hwwanalysis-0/
```


Now you can start the container on the cluster and run the prepare and initialize steps.


```
NEMO> pwd
/work/ws/nemo/<nemo-user>-hwwanalysis-0/run
NEMO> module load tools/singularity
NEMO> singularity shell --containall\
 -B /tmp:/tmp,$PWD:/hww/run,/work:/work docker://gitlab-registry.cern.ch/USERNAME/REPOSITORY/hwwanalysis:latest
 
```

If you send the container directly to the cluster execute the local singularity image, instead
of pulling the container from the registry. 

```
NEMO> singularity shell --containall -B /tmp/:/tmp/,$PWD:/hww/run,/work:/work ../hwwanalysis.simg
```

To run the prepare and initialize steps execute the following within the container:

```
NEMO_container> source /home/atlas/release_setup.sh
NEMO_container> cd /hww/run
NEMO_container> source ../HWWAnalysisCode/setup/setupAnalysis.sh
NEMO_container> prepare.py config/master/lvqq/prepare-lvqq-2018.cfg
NEMO_container> initialize.py config/master/lvqq/initialize-lvqq-2018.cfg
```

Back in the local docker container download the submission script and controller and rsync
the prepare and initialize output into the run folder.

```
container> cd /hww/run
container> sudo chown -R atlas ../.
container> git clone https://:@gitlab.cern.ch:8443/jmatusza/cafdockercontainer.git
container> mv cafdockercontainer/* . 
container> rm -rf cafdockercontainer README.md
container> mv container.py ../HWWAnalysisCode/CAFCore/CommonAnalysisHelpers/python/submissionControllers/.
container> rsync <nemo-user>@login1.nemo.uni-freiburg.de:/work/ws/nemo/<nemo-user>-hwwanalysis-0/run/sampleFolders . -vhPa
```

The submission depends on the container used and the directory in which the project is mounted,
so we have to modify the my_sub.py script. 
In line 38 modify the `container` variable to match the name of your container

```python
container = "docker://gitlab-registry.cern.ch/USERNAME/REPOSITORY/hwwanalysis:latest"
```

or the path of the image on the cluster.

```
container = "/work/ws/nemo/<nemo-user>-hwwanalysis-0/hwwanalysis.simg"
```


The script uses a template command to start the singularity containers on the worker nodes.
Besides the /home directory the container has no access to the hosts file system, so 
if the analysis needs access to e.g. the `/cvmfs` folder on the cluster simply extend the mount option in the 
my_sub.py script with `-B /cvmfs:/cvmfs`.

```python
mount =  "-B <source1>:<target1>,<source2>:<target2>"
```

Finally we can submit the jobs via the my_sub.py script.

```
container> export NUSER=<nemo-user>
container> python my_sub.py config/master/lvqq/analyze-lvqq-2018.cfg --jobs config/jobLists/lvqq/jobs.txt -w /work/ws/nemo/$NUSER-hwwanalysis-0/run --host $NUSER@login1.nemo.uni-freiburg.de --identifier v1
```

